# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings

class Head(models.Model):
    category = models.CharField(max_length=150, unique=True, default="", blank=True, verbose_name=u'Рубрика')

    def __unicode__(self):
        return '%s' % (self.category)

    class Meta:
        verbose_name_plural = u'Рубрики'
        verbose_name = u'Рубрика'

class Post(models.Model):
    head =  models.ForeignKey(Head, null=True, blank=True, verbose_name=u'Рубрика')
    post_title = models.CharField(max_length=150, unique=True, blank=True, default="", verbose_name=u'Заголовок')
    full_text = models.TextField(max_length=600, blank=True, default="", verbose_name=u'Текст')
    pub_date = models.CharField(max_length=150, blank=True, default="", verbose_name=u'Дата публикации')
    image = models.ImageField(upload_to=settings.IMAGE_UPLOAD_DIR, default="",  blank=True,  verbose_name=u'Изображение')
    published = models.BooleanField(blank=True, verbose_name=u'Опубликован')

    def __unicode__(self):
        return '%s' % (self.post_title)

    class Meta:
        verbose_name_plural = u'Посты'
        verbose_name = u'Пост'

class Feed(models.Model):
    feed_title = models.CharField(max_length=150, default="", blank=True, verbose_name=u'Название')
    url = models.URLField(max_length=250, default="", blank=True, verbose_name='url')

    def __unicode__(self):
        return '%s' % (self.feed_title)

    class Meta:
        verbose_name_plural = u'Фиды'
        verbose_name = u'Фид'
