# -*- coding: utf-8 -*-

from django.shortcuts import render
from magnetfeeds.apps.publication.models import Post
# from lxml import etree
# from django.conf import settings
# from django.http import HttpResponse, Http404,HttpResponseRedirect


def index(request):
    posts = Post.objects.all()
    
    title = 'Publications'
    context = {'title': title, 'posts': posts}
 
    return render(request, 'publication/index.html', context)
