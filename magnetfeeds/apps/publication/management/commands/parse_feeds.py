# -*- coding: utf-8 -*-

from django.core.management.base import NoArgsCommand
from HTMLParser import HTMLParser
from django.conf import settings
import feedparser
import psycopg2
import sys
import os

from magnetfeeds.apps.publication.management.commands.querydb import OueryDb

class ParseHtml(HTMLParser):
    line = ''

    def handle_data(self, data):
        ParseHtml.line += data + ' '

    def getLine(self):
        return ParseHtml.line

    def zeroLine(self):
        ParseHtml.line = ''
        return ParseHtml.line


class Command(NoArgsCommand):
    # option_list = AppCommand.option_list + (
    #     make_option('--count', action='store_true', dest='count', default= False,
    #         help='Add object count information' ),
    # )
    help = 'Parser feeds in RSS 2.0 format or Atom and create posts of them'
    # args = '[appname ...]'

    requires_model_validation = True

    def handle_noargs(self, **options):
        query_db = OueryDb("db_magnet", "127.0.0.1", "5432", "123", "magnet")
        url_feed = query_db.pathFeedUrl()

        # Parser feed
        fp = feedparser.parse(url_feed)

        for ln in fp.entries:
            image = ''
            try:
                descript = str(ln.description.encode('UTF-8')).split(" ")
                for i in descript:
                    if 'http:' in i:
                        image = i.split('"')[1]
                        break
                    else:
                        image = "Not images"
            except:
                image = "Not images"

            #  Title Post
            try:
                title = ln.title.encode("utf-8")
            except AttributeError:
                title = 'No title'

            #  FULLTEXT Post
            try:
                text = ln.fulltext.encode("utf-8")
            except AttributeError:
                text = 'No text'

            #  DATA Post
            try:
                data = ''
                tmp = ln.published.encode("utf-8").split(' ')[:-1]
                for i in tmp:
                    data += i + ' '
            except AttributeError:
                data = 'No data'

            # CHECKOUT Post
            published = 'True'

            # Parsing FULL TEXT
            full_text = ''
            parser = ParseHtml()
            parser.feed(text)
            full_text = parser.getLine()
            parser.zeroLine()

            # Write CATEGORY Post
            category_post = ln.category.encode("utf-8")
            query_db.writeCategoryDb(category_post)

            # Get CATEGORY-ID
            id_head = int(query_db.getHeadId(category_post))

            #  Write to table "public.publication_post"
            query_db.writeToDataBase(title, full_text, data, image, published, id_head)

        query_db.closeDb()
