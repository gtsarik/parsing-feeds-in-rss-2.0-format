# -*- coding: utf-8 -*-

import psycopg2
import sys
import os

class OueryDb(object):
    def __init__(self, database, host, port, password, user):
        self.__database = database
        self.__host = host
        self.__port = port
        self.__password = password
        self.__user = user
        self.__connect = None
        self.__cursor = None

    def __connectDb(self):
        try:
            self.__connect = psycopg2.connect(database=self.__database, host=self.__host,
                port=self.__port, password=self.__password, user=self.__user)  
            self.__cursor = self.__connect.cursor()
        except psycopg2.DatabaseError, e:
            if self.__connect:
                self.__connect.rollback()
            print 'Error %s' % e    
            sys.exit(1)

    def pathFeedUrl(self):
        try:
            url_feed = None
            query_url = '''
                    SELECT url FROM publication_feed
                    ORDER BY id DESC
                    LIMIT 1;
                    '''
            if not self.__connect:
                self.__connectDb()
            self.__cursor.execute(query_url)
            url_feed = str(self.__cursor.fetchone()[0])
            self.__connect.commit()
        except psycopg2.DatabaseError, e:
            if self.__connect:
                self.__connect.rollback()
            print 'Error %s' % e    
            sys.exit(1)
        return url_feed

    def writeCategoryDb(self, name):
        if not self.getHeadId(name):
            try:
                query_category = '''
                        INSERT INTO publication_head (category)
                        VALUES ('%s');
                        ''' % name

                if not self.__connect:
                    self.__connectDb()
                self.__cursor.execute(query_category)
                self.__connect.commit()
            except psycopg2.DatabaseError, e:
                message = '''
                        duplicate key value violates unique constraint
                        "publication_head_category_uniq"
                        '''
                if e not in message:
                    if self.__connect:
                        self.__connect.rollback()
                    print 'Error: %s' % e    
                    sys.exit(1)

    def getHeadId(self, name):
        try:
            id_head = None
            query_id_head = '''
                    SELECT id FROM publication_head
                    WHERE category = '%s';
                    ''' % name

            if not self.__connect:
                self.__connectDb()
            self.__cursor.execute(query_id_head)
            id_head = str(self.__cursor.fetchone()[0])
            self.__connect.commit()
        except TypeError, e:
            print 'Error: %s' % e 
            id_head = None

        except psycopg2.DatabaseError, e:
            if self.__connect:
                self.__connect.rollback()
            print 'Error: %s' % e 
            sys.exit(1)

        return id_head

    def writeToDataBase(self, title, text, data, image, published, head_id):
        try:
            query = '''
                    INSERT INTO publication_post (post_title, full_text, pub_date, image, published, head_id)
                    VALUES ('%s', '%s', '%s', '%s', '%s', %d);
                    ''' % (title, text, data, image, published, head_id)

            if not self.__connect:
                self.__connectDb()
            self.__cursor.execute(query)
            self.__connect.commit()
        except psycopg2.DatabaseError, e:
            if self.__connect:
                self.__connect.rollback()
            print 'Error: %s' % e

    def closeDb(self):
        if self.__connect:
            self.__connect.rollback()
        sys.exit(1)
