# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FeedFile'
        db.create_table(u'publication_feedfile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rss_file', self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True)),
        ))
        db.send_create_signal(u'publication', ['FeedFile'])

        # Deleting field 'Feed.rss_file'
        db.delete_column(u'publication_feed', 'rss_file')


    def backwards(self, orm):
        # Deleting model 'FeedFile'
        db.delete_table(u'publication_feedfile')

        # Adding field 'Feed.rss_file'
        db.add_column(u'publication_feed', 'rss_file',
                      self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True),
                      keep_default=False)


    models = {
        u'publication.feed': {
            'Meta': {'object_name': 'Feed'},
            'feed_title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'default': "''", 'max_length': '250', 'blank': 'True'})
        },
        u'publication.feedfile': {
            'Meta': {'object_name': 'FeedFile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rss_file': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'})
        },
        u'publication.head': {
            'Meta': {'object_name': 'Head'},
            'category': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'publication.post': {
            'Meta': {'object_name': 'Post'},
            'full_text': ('django.db.models.fields.TextField', [], {'default': "''", 'max_length': '600', 'blank': 'True'}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['publication.Head']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'post_title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            'pub_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['publication']