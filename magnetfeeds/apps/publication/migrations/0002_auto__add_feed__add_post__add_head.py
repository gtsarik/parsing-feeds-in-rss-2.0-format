# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Feed'
        db.create_table(u'publication_feed', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('feed_title', self.gf('django.db.models.fields.CharField')(default='', max_length=150, blank=True)),
            ('url', self.gf('django.db.models.fields.CharField')(default='', max_length=250, blank=True)),
        ))
        db.send_create_signal(u'publication', ['Feed'])

        # Adding model 'Post'
        db.create_table(u'publication_post', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('post_title', self.gf('django.db.models.fields.CharField')(default='', max_length=150)),
            ('full_text', self.gf('django.db.models.fields.TextField')(default='', max_length=600)),
            ('pub_date', self.gf('django.db.models.fields.DateField')(default='', auto_now=True, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True)),
            ('published', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'publication', ['Post'])

        # Adding model 'Head'
        db.create_table(u'publication_head', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.CharField')(default='', max_length=150, blank=True)),
        ))
        db.send_create_signal(u'publication', ['Head'])


    def backwards(self, orm):
        # Deleting model 'Feed'
        db.delete_table(u'publication_feed')

        # Deleting model 'Post'
        db.delete_table(u'publication_post')

        # Deleting model 'Head'
        db.delete_table(u'publication_head')


    models = {
        u'publication.feed': {
            'Meta': {'object_name': 'Feed'},
            'feed_title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250', 'blank': 'True'})
        },
        u'publication.head': {
            'Meta': {'object_name': 'Head'},
            'category': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'publication.post': {
            'Meta': {'object_name': 'Post'},
            'full_text': ('django.db.models.fields.TextField', [], {'default': "''", 'max_length': '600'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'post_title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150'}),
            'pub_date': ('django.db.models.fields.DateField', [], {'default': "''", 'auto_now': 'True', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['publication']