# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Post', fields ['post_title']
        db.create_unique(u'publication_post', ['post_title'])


    def backwards(self, orm):
        # Removing unique constraint on 'Post', fields ['post_title']
        db.delete_unique(u'publication_post', ['post_title'])


    models = {
        u'publication.feed': {
            'Meta': {'object_name': 'Feed'},
            'feed_title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'default': "''", 'max_length': '250', 'blank': 'True'})
        },
        u'publication.head': {
            'Meta': {'object_name': 'Head'},
            'category': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '150', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'publication.post': {
            'Meta': {'object_name': 'Post'},
            'full_text': ('django.db.models.fields.TextField', [], {'default': "''", 'max_length': '600', 'blank': 'True'}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['publication.Head']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'post_title': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '150', 'blank': 'True'}),
            'pub_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['publication']