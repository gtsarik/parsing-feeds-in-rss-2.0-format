# -*- coding: utf-8 -*-
from django.contrib import admin
 
from magnetfeeds.apps.publication.models import Head, Post, Feed
 
class HeadAdmin(admin.ModelAdmin):
    list_display = ['category']
    list_filter = ('category',)

class PostAdmin(admin.ModelAdmin):
    list_display = ['post_title', 'full_text', 'pub_date',
        'image', 'published']
    list_filter = ('post_title', 'pub_date',)

class FeedAdmin(admin.ModelAdmin):
    list_display = ['feed_title', 'url']
    list_filter = ('feed_title', 'url',)
 
admin.site.register(Head, HeadAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Feed, FeedAdmin)
