django==1.5.5
psycopg2==2.5.1
pillow==2.2.1
south==0.8.2
django-suit==0.2.5
unittest2==0.5.1
selenium==2.37.2
mock==1.0.1
